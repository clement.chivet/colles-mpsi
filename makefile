.PHONY:
	all
all:
	ls -v exercices/$(week)/*.tex | grep -Ev "^exercices/$(week)/!" | awk -v pwd="$$PWD" '{if ($$1!="exercices/$(week)/base.tex") printf "\\input{"pwd"/%s}\n", $$1}' > inputs.tex && nvim colles.tex
