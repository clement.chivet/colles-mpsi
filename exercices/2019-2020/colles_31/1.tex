\exo{Théorème de réarrangement de Riemann}
Soit $(u_n)$ une suite réelle dont la série associée est semi-convergente, ie 
\begin{equation*}
  \sum_{k=0}^n{u_k} \underset{n \to \infty}{\longrightarrow} l \in \R \text{ mais } \sum_{k=0}^\infty{|u_k|} = + \infty
\end{equation*}
Alors pour tout $\alpha \in \R \cup \{ - \infty, + \infty \}$, il existe une permutation $\sigma$ de $\N$ telle que 
\begin{equation*}
  \sum_{k=0}^{n}{u_{\sigma(k)}} \underset{n \to \infty}{\longrightarrow} \alpha
\end{equation*}




\ifwithcorrection \correction 
Soit $\alpha \in \R$.

Tout d'abord, si on note $I^+ = \{ n \in \N, u_n \geq 0\}$, et $I^- = \{ n \in \N, u_n < 0\}$, on a une partition de $\N$ en deux ensembles infinis : si par l'absurde $I^+$ est fini, alors $\sum u_n$ est une somme de termes négatifs à partir d'un certain rang, donc sa convergence équivaut à la convergence de $\sum |u_n|$, ce qui est absurde par l'hypothèse, et de même pour $I^-$. 

D'autre part, on a $\sum_{i \in I^+}{ u_i}$ (qui est une somme de termes positifs, et c'est une écriture simplifiée pour dire $\sum_{i=0}^\infty{u_{\varphi(n)}}$ où $\varphi : \N \to I^+$ bijection strictement croissante) qui diverge :  
\begin{equation*}
  \sum_{i\in I^+}{u_i} = \sum_{i=0}^\infty{\max(0,u_i)} = \sum_{i=0}^\infty{\frac{u_i +|u_i|}{2}} = + \infty
\end{equation*}
(On se permet d'écrire les sommes même si elles ne convergent pas car elles sont à termes positifs, donc on peut toujours voir $\sum {|a_i|} \in \R_+\cup \{ + \infty\}$). On a de même pour le cas négatif.



On construit ensuite la bijection : 
On pose $\sigma(0) = 0$, puis on construit $\sigma(n)$ par induction/récurrence : 

On suppose construits les $\sigma(0),\ldots,\sigma(n-1)$, 
\begin{itemize}
  \item Si $\sum_{i=0}^{n-1}u_{\sigma(i)} \leq \alpha$, on ajoute le prochain terme positif pas encore utilisé, ie $\sigma(n) = \min I^+ \setminus \{ \sigma(k), 0 \leq k \leq n-1 \}$. $\sigma(n)$ est bien défini car $I^+$ est infini, donc reste une partie non vide de $\N$ quand on lui enlève un nombre fini d'éléments, donc le min est bien défini.
  \item Sinon, c'est que $\sum_{i=0}^{n-1}u_{\sigma(i)} > \alpha$, et donc on pose de manière symétrique $\sigma(n) = \min I^- \setminus \{ \sigma(k), 0 \leq k \leq n-1 \}$.
\end{itemize}


Par définition, $\sigma : \N \to \N$ est injective. Montrons qu'elle est bien surjective : supposons par l'absurde qu'il existe $n_0 \in \N \setminus \sigma(\N)$, et choisissons $n_0$ minimal pour cette propriété. Disons de plus $n_0 \in I^+$, l'autre cas étant identique. Alors pour tout $n > n_0, n \in I^+$, on a $n \not\in \sigma(\N)$, car si c'était le cas,
$n = \sigma(k)$, cela proviendrait d'une étape de construction $\sigma(k) = \min I^+ \setminus \{ \sigma(i), 1 \leq i \leq k-1 \}$, mais $n_0$ n'appartient par au second ensemble, donc est bien dans $I^+ \setminus \{ \sigma(i), 1 \leq i \leq k\}$, ce qui contredit le fait que $n$ est le min de cet ensemble, absurde. 

Alors l'ensemble $I^+ \cap \sigma(\N)$ est fini (car inclu dans $[|0,n_0 |]$), donc à partir d'un certain rang $N_0$, on a $\forall n \geq N_0,\sigma(n) \in I^-$. Mais alors pour tout $N$ assez grand, on a 
\begin{equation*}
  \sum_{k=0}^N {u_{\sigma(k)}} = \underbrace{\sum_{k \in I^+ \cap \sigma(\N)}{u_{\sigma(k)}}}_{\in \R, \text{ constante }} + \sum_{k = 0}^{\sigma(N)}{\min(0,u_k)}
\end{equation*}
où on a regroupé dans l'équation précédente les termes positifs, en nombre fini, et les termes négatifs, qui sont par définition de $\sigma$ tous les négatifs qui arrivent avant $\sigma(N)$. Mais comme $\sum_{k = 0}^{\sigma(N)}{\min(0,u_k)} \underset{N\to \infty}{\longrightarrow} - \infty$, on a à partir d'un certain rang $ \sum_{k=0}^N {u_{\sigma(k)}} \leq \alpha$, et donc par définition de $\sigma$, on a $\sigma(N+1) = \min I^+ \setminus \sigma(N) = n_0$ (car on a choisi $n_0$ minimal). Absurde ! D'où $\sigma$ est bien une bijection.


Montrons enfin que $\sum_{n=0}^N{u_{\sigma(n)}} \underset{N\to \infty}{\longrightarrow} \alpha$ :

Soit $\epsilon > 0$. Comme $\sum u_i$ est semi convergente, on a $u_i \to 0$, donc il existe un rang $n_0$ à partir duquel $|u_n| \leq \epsilon$. L'ensemble $\{n, \sigma(n) \leq n_0\}$ est fini par injectivité, donc majoré strictement pas un certain entier $n_1$, qui vérifie donc que $\forall n \geq n_1, |u_{\sigma(n)}|\leq \epsilon$. De plus, comme $\sigma$ est surjective et que $I^+,I^-$ sont infinis, il existe $N_0\geq n_1$ tel que $\sigma(N_0) \in I^+, \sigma(N_0 + 1) \in I^-$ (ou alors en reprenant la preuve par l'absurde précédente). Montrons que pour $n \geq N_0, \left|\sum_{k=0}^n{u_{\sigma(k)}} - \alpha \right| \leq \epsilon$ :
\begin{itemize}
  \item c'est vrai en $N_0$ car comme il y a changement de signe, c'est que $\sum_{k=0}^{N_0-1}{u_{\sigma(k)}} \leq \alpha$, mais $\sum_{k=0}^{N_0}{u_{\sigma(k)}} > \alpha$. Comme $|u_{\sigma(N_0)}| \leq \epsilon$, on a donc bien forcément $\left|\sum_{k=0}^{N_0}{u_{\sigma(k)}} - \alpha \right|\leq \epsilon$.
  \item Supposons que c'est vrai jusqu'au rang $N \geq N_0$. Si $\sum_{k=0}^{N}{u_{\sigma(k)}} \in [\alpha - \epsilon,\alpha]$, alors pour passer à $N+1$ on ajoute $a_{\sigma(N-1)}$ qui est un terme positif plus petit que $\epsilon$ : on reste bien dans $[ \alpha - \epsilon, \alpha + \epsilon]$. De même pour le cas $\sum_{k=0}^{N}{u_{\sigma(k)}} \in ]\alpha,\alpha + \epsilon]$. 
\end{itemize}

Par récurrence on a donc bien pour tout $n \geq N_0, \left|\sum_{k=0}^n{u_{\sigma(k)}} - \alpha \right| \leq \epsilon$. D'où la convergence voulue.


Le cas $\pm \infty$ se traite à peu près de la même manière.

\fi
