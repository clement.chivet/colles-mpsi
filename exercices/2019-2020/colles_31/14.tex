\exo{Sommation d'équivalents}
Soient $(a_n)_{n\in\N}$ et $(b_n)_{n\in \N}$ deux suites à termes positifs telles que $a_n \sim b_n$. En revenant à la définition en $\epsilon$ :
\q Si $\sum a_n$ diverge, alors
\begin{equation*}
\sum_{k=0}^n{a_k} \sim_{n\to \infty} \sum_{k=0}^n{b_k}
\end{equation*}
\q Si $\sum a_n$ converge, alors $\sum b_n$ aussi et
\begin{equation*}
\sum_{k=n+1}^\infty{a_k} \sim_{n\to \infty} \sum_{k=n+1}^\infty{b_k}
\end{equation*}

\ifwithcorrection \correction
Soient $\left(a_{n}\right)_{n \in N}$ et $\left(b_{n}\right)_{n \in \mathbb{N}}$ deux suites positives telles que $a_{n} \sim b_{n}$. Nous savons déjà (cf. cours) que $\sum a_{n}$ et $\sum b_{n}$ sont de mêne nature. 

\q Pour $n \in \mathbb{N}$, notons $A_{n}=\sum_{k=0}^{n} a_{k}$ et $B_{n}=\sum_{k=0}^{n} b_{k}$. Montrons que les sommes partielles sont équivalentes. 

Commes les deux séries positives divergent, $\lim _{n \infty} A_{n}=\lim _{n \infty} B_{n}=+\infty$. Nous allons montrer que $\lim _{n \infty} \frac{A_{n}}{B_{n}}=1$ en revenant à la définition. Soit $\varepsilon>0 .$

Comme $\lim _{n \infty} \frac{a_{n}}{b_{n}}=1$, il existe $n_{1} \in \mathbb{N}$ tel que $\forall n \geqslant n_{1},\left|\frac{a_{n}}{b_{n}}-1\right| \leqslant \frac{\varepsilon}{2}$, i.e. $\left|a_{n}-b_{n}\right| \leqslant \frac{\varepsilon}{2} b_{n}$
Comme $\lim _{n \infty} B_{n}=+\infty$, on a $\lim _{n \infty} \frac{\left|A_{n_{1}-1}-B_{n_{1}-1}\right|}{B_{n}}=0$
donc il existe $n_{2} \in \mathbb{N}$ tel que $\forall n \geqslant n_{2}, \frac{\left|A_{n_{1}-1}-B_{n_{1}-1}\right|}{B_{n}} \leqslant \frac{\varepsilon}{2}$.
Posons $n_{0}=\max \left(n_{1}, n_{2}\right) \in \mathbb{N} .$ Soit $n \geqslant n_{0}$.


$\left|\frac{A_{n}}{B_{n}}-1\right|=\left|\frac{A_{n}-B_{n}}{B_{n}}\right| \leqslant \frac{\left|A_{n_{1}-1}-B_{n_{1}-1}\right|}{B_{n}}+\frac{1}{B_{n}} \sum_{k=n_{1}}^{n}\left|a_{k}-b_{k}\right|$ par inégalité triangulaire. Or, $n \geqslant n_{2}$ donc
$\frac{\left|A_{n_{1}-1}-B_{n_{1}-1}\right|}{B_{n}} \leqslant \frac{\varepsilon}{2}$ et $\forall k \in \llbracket n_{1}, n \rrbracket,\left|a_{k}-b_{k}\right| \leqslant \frac{\varepsilon}{2} b_{k}$ d'où $\sum_{k=n_{1}}^{n}\left|a_{k}-b_{k}\right| \leqslant \frac{\varepsilon}{2} \sum_{k=n_{1}}^{n} b_{k} \leqslant \frac{\varepsilon}{2} B_{n}\left(\operatorname{car} \sum b_{n}\right.$ est
à termes positifs). Ainsi $\left|\frac{A_{n}}{B_{n}}-1\right| \leqslant \varepsilon$. On a donc montré que $\forall \varepsilon>0, \exists n_{0} \in \mathbb{N}, \forall n \geqslant n_{0},\left|\frac{A_{n}}{B_{n}}-1\right| \leqslant \varepsilon$, i.e. $\lim _{n \infty} \frac{A_{n}^{\prime}}{B_{n}}=1$ soit $A_{n} \sim B_{n}$.
Ainsi, en cas de divergence, $A_{n} \sim B_{n}$.


En fait on a juste repris la preuve de Césaro. 

\q Montrons que les restes sont équivalents : Soit $\epsilon > 0$. Comme $a_n \sim b_n$, il existe un $n_0$ tel que $\forall k \geq n_0, |a_k - b_k| \leq \epsilon b_k$. 

Soit $n \geq n_0$. Pour tout $M \geq n,$
\begin{equation*}
  \left| \frac{\sum_{k=n}^M{a_k}}{\sum_{k=n}^M{b_k}}  - 1 \right| \leq \frac{\sum_{k=n}^M{|a_k-b_k|}}{\sum_{k=n}^M b_k} \leq \epsilon
\end{equation*}
D'où le résultat en faisant $M \to \infty$.




\begin{Rq}
  Cet exercice sera du cours en seconde année, et est très utile et très général. Il permet de trouver un équivalent d'une suite $u_n$, surtout quand cette suite est une somme partielle. En effet, il suffit de voir que $u_n$ a le même comportement que la série des $u_{n+1}-u_n$, trouver un équivalent de ce terme en une somme de riemann par exemple, puis conclure.

  En reprenant la preuve de l'équivalent de stirling, dans lequel on pose $u_n = \ln(n!) - n\ln n+n-\frac{1}{2} \ln n$, que l'on montre que la série $\sum u_{n+1} - u_n$ est convergente, de limite trouvée grâce aux intégrales de Wallis $\ln \sqrt{2\pi}$, on peut ensuite écrire
  \begin{equation*}
    u_n-\ln\sqrt{2\pi} = \sum_{k=n}^\infty {(u_{k+1}-u_k)}
  \end{equation*}
  Mais $u_{n+1}-u_n \sim \frac{1}{12n^2} \sim \frac{1}{12n(n+1)} \sim \frac{1}{12} \left( \frac{1}{n} - \frac{1}{n+1} \right)$. En appliquant le théorème de sommation des équivalents, on trouve alors 
  \begin{equation*}
    u_n - \ln\sqrt{2\pi} = \sum_{k=n}^\infty {(u_{k+1} - u_k)} \sim \sum_{k=n}^{\infty}\frac{1}{12}\left( \frac{1}{k} - \frac{1}{k+1} \right) = \frac{1}{12n} 
  \end{equation*}
  Finalement, cela permet d'obtenir 
  \begin{equation*}
    n! \underset{n \to \infty}{=} \left( \frac{n}{e} \right)^n \sqrt{2\pi n}\left( 1+ \frac{1}{12n} + o\left( \frac{1}{n} \right)  \right)   
  \end{equation*}
\end{Rq}



\fi
